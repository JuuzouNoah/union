<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Tipo_UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_usuarios')->insert([
            [
                'Nombre' => 'Administrador',
                'Descripcion' => 'Supervisa la actividad de los moderadores.',
                "created_at" => Carbon::now()
            ],
            [
                'Nombre' => 'Moderador',
                'Descripcion' => 'Atiende los reportes de los usuarios.',
                "created_at" => Carbon::now()
            ],
            [
                'Nombre' => 'Cliente',
                'Descripcion' => 'Hace uso de las funciones cliente de la plataforma.',
                "created_at" => Carbon::now()
                ]
        ]);
    }
}
