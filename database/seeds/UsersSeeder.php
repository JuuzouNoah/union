<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use \Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            [
                'name' => 'Noé Jafet Vela Pat',
                'email' => 'xxel.chelitoxx@gmail.com',
                'password' => Hash::make('123456789'),
                'FK_Tipo_Usuario' => 1,
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'Leonardo Balderas Montiel',
                'email' => 'leobalderas@gmail.com',
                'password' => Hash::make('123456789'),
                'FK_Tipo_Usuario' => 1,
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'Kenia Saraí Alcántara Hernández',
                'email' => 'kenalcantara05@gmail.com',
                'password' => Hash::make('123456789'),
                'FK_Tipo_Usuario' => 1,
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'Usuario Moderador',
                'email' => 'usuariomoderador@gmail.com',
                'password' => Hash::make('123456789'),
                'FK_Tipo_Usuario' => 2,
                'created_at' => Carbon::now()
            ],
        ]);
    }
}
