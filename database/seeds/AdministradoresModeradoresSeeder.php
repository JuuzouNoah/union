<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use \Illuminate\Support\Facades\DB;

class AdministradoresModeradoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('administradores')->insert([
            [
                'FK_User' => 1,
                "created_at" => Carbon::now()
            ],
            [
                'FK_User' => 2,
                "created_at" => Carbon::now()
            ],
            [
                'FK_User' => 3,
                "created_at" => Carbon::now()
            ]
        ]);

        DB::table('moderadores')->insert([
            [
                'FK_User' => 4,
                "created_at" => Carbon::now()
            ],
        ]);
    }
}
