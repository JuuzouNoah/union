<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller
{
    //
    public function inicio()
    { 
        return view('pages-web.inicio');
    }

    public function preguntas()
    { 
        return 'Apartado preguntas';
    }

    public function blogs()
    { 
        return 'Apartado blogs';
    }

    public function asesores()
    { 
        return 'Apartado asesores';
    }
}
