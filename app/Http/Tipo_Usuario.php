<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_Usuario extends Model
{    
    /**
     * El nombre de la tabla, puesto así por el guión bajo que da problemas.
     *
     * @var array
     */
    protected $table = 'tipo_usuarios';
}
