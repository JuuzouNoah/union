<?php

namespace App\Http\Middleware;

use Closure;

class RolMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->FK_Tipo_Usuario == 3) {
            abort(401, 'This action is unauthorized, you got Cliente role.');
        }
        
        return $next($request);
    }
}
