<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

// USUARIO INVITADO
Route::get('/', 'AppController@inicio');
Route::get('/preguntas', 'AppController@preguntas');
Route::get('/blogs-practicos', 'AppController@blogs');
Route::get('/asesores', 'AppController@inicio');

// CLIENTE, ADMIN Y MODERADOR
Route::group(['middleware' => ['sesion']], function(){

    Route::get('/actividad', function(){
        return 'Apartado actividad';
    });

    Route::get('/mi-contenido', function(){
        return 'Apartado mi contenido';
    });

    Route::get('/perfil', function(){
        return 'Apartado perfil';
    });

});


// ADMIN Y MODERADOR
Route::group(['middleware' => ['sesion', 'rol']], function(){

    Route::get('/admin', 'AdminController@inicio');

    Route::get('/admin/reportes', function(){
        return 'Apartado reportes';
    });

    Route::get('/admin/casos-cerrados', function(){
        return 'Apartado casos cerrados';
    });

});



// ADMIN
Route::group(['middleware' => ['sesion', 'rol-admin']], function(){

    Route::get('/admin/actividad-usuarios', function(){
        return 'Apartado actividad usuarios';
    });

});

