<!--Navbar -->
<nav class="mb-1 navbar navbar-expand-lg navbar-dark default-color">
    <a class="navbar-brand" href="/sucursales-crud-react/public">Panel Administrativo</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
        aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

</nav><!--/.Navbar --><!--Navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-light white double-nav scrolling-navbar">
    <div class="container">
        <a class="navbar-brand" href="/admin">Panel Administrativo</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-1">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent-1">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item @if (Request::path() == '/admin') {{'active'}} @endif">
                    <a class="nav-link" href="/admin">Inicio
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item @if (Request::path() == '/admin/reportes') {{'active'}} @endif">
                    <a class="nav-link" href="/admin/reportes">Reportes
                    </a>
                </li>
                <li class="nav-item @if (Request::path() == '/admin/casos-cerrados') {{'active'}} @endif">
                    <a class="nav-link" href="/admin/casos-cerrados">Casos Cerrados
                    </a>
                </li>  
                @if (Auth::user()->FK_Tipo_Usuario == 1)
                    <li class="nav-item @if (Request::path() == '/admin/actividad-usuarios') {{'active'}} @endif">
                        <a class="nav-link" href="/admin/actividad-usuarios">Actividad Usuarios
                        </a>
                    </li> 
                @endif
            </ul>
            
            <ul class="navbar-nav ml-auto nav-flex-icons">
                <li class="nav-item">
                    <a href="/" class="nav-link waves-effect waves-light">
                        <i class="fas fa-globe"></i>                    
                    </a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-default"
                        aria-labelledby="navbarDropdownMenuLink-333">
                        <a href="#" class="dropdown-item">{{Auth::user()->name}}</a>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar sesión</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                                </form>

                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!--/.Navbar -->

