{{-- Extensión del layout padre del sitio web --}}
@extends('layouts-web.app')

{{-- Aplicación de Yield --}}
@section('titulo', 'Inicio - Unión')

{{-- Aplicación de Yield --}}
@section('contenido')
    <!-- Jumbotron -->
    <div class="jumbotron text-center mt-6 mb-5">

        <!-- Title -->
        <h2 class="card-title h2">Inicio Web</h2>
        <!-- Subtitle -->
        <h5 class="blue-text my-4 font-weight-bold">
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Porro nesciunt tempora sapiente ducimus earum natus, odio numquam aperiam eum nihil tempore fugit beatae molestiae nostrum officiis expedita at rem accusamus.
        </h5>

        <!-- Grid row -->
        <div class="row d-flex justify-content-center">

            <!-- Grid column -->
            <div class="col-xl-7 pb-2">

            <p class="card-text">
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae obcaecati dolorum aspernatur odit, debitis, ratione deserunt minus ad velit mollitia cum eius reprehenderit. Minima ut eum labore ea eligendi necessitatibus?
                <br>
            </p>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <hr class="my-4">

        <div class="pt-2">
            <a href="/" class="btn btn-cyan waves-effect">lorem ipsum</a>
        </div>
        <div class="pt-2">
            <a href="/" class="btn btn-dark waves-effect">lorem ipsum</a>
        </div>

    </div>
<!-- Jumbotron -->
<br><br><br><br><br><br><br><br><br><br>
@endsection

