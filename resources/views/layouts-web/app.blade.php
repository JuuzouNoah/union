<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>
        {{-- Yield para el título de la página, se usa en las otras vistas --}}
        @yield('titulo')
    </title>

    {{-- Yield para que cada página incruste sus estilos propios --}}
    @yield('estilos')

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('css/Web/bootstrap.css')}}" rel="stylesheet">    
    <!-- Material Design Bootstrap -->
    <link href="{{asset('css/Web/mdb.css')}}" rel="stylesheet">
</head>

<body>
    {{-- Include para incluir la barra de navegación --}}
    <div class="container">
        @include('layouts-web.navbar')
    </div>

    {{-- Yield para incrustar el contenido de cada página --}}
    <div class="container">
        @yield('contenido')
    </div>

    {{-- Include para incluir el pie de página --}}
    <div>
        @include('layouts-web.footer')
    </div>

    <!-- JQuery -->
    <script type="text/javascript" src="{{asset('js/Web/jquery-3.4.1.min.js')}}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{asset('js/Web/popper.min.js')}}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{asset('js/Web/bootstrap.min.js')}}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{asset('js/Web/mdb.js')}}"></script>
</body>

<script src="{{ asset('js/app.js') }}" ></script>


</html>