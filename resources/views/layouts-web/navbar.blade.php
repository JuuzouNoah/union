<!--Navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-light white double-nav scrolling-navbar">
    <div class="container">
        <a class="navbar-brand" href="/">Página Web</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-1">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent-1">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item @if (Request::path() == '/') {{'active'}} @endif">
                    <a class="nav-link" href="/">Inicio
                        <span class="sr-only">(current)</span>
                    </a>
                </li>

                <li class="nav-item dropdown @if (Request::path() == '/ramas-estudio') {{'active'}} @endif">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">Ramas de Estudio
                    </a>
                    <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
                        <a class="dropdown-item" href="#">Una rama</a>
                        <a class="dropdown-item" href="#">Otra rama</a>
                        <a class="dropdown-item" href="#">Otra rama aquí</a>
                        <a class="dropdown-item" href="#">Una rama más</a>
                        <a class="dropdown-item" href="#">La última rama</a>

                    </div>
                </li>

                <li class="nav-item @if (Request::path() == '/preguntas') {{'active'}} @endif">
                    <a class="nav-link" href="/preguntas">Preguntas
                    </a>
                </li>
                <li class="nav-item @if (Request::path() == '/blogs-practicos') {{'active'}} @endif">
                    <a class="nav-link" href="/blogs-practicos">Blogs Prácticos
                    </a>
                </li>
                <li class="nav-item @if (Request::path() == '/asesores') {{'active'}} @endif">
                    <a class="nav-link" href="/asesores">Asesores
                    </a>
                </li>        
            </ul>

            <ul class="navbar-nav ml-auto nav-flex-icons">
                <form class="form-inline">
                    <div class="md-form my-0">
                        <input class="form-control mr-sm-2" type="text" placeholder="Preguntas - Blogs" aria-label="Preguntas - Blogs">
                    </div>
                </form>

                    {{-- Invitado --}}
                    @guest
                    <li class="nav-item">
                        <a href="/login" class="nav-link waves-effect waves-light">
                            <i class="fas fa-user"></i>
                        </a>
                    </li>

                    @else

                    @if (Auth::user()->FK_Tipo_Usuario != 3)
                            <li class="nav-item">
                                <a class="nav-link waves-effect waves-light" href="/admin">
                                    <i class="fas fa-th-large"></i>
                                </a>
                            </li>
                    @endif
                    {{-- Sesión iniciada --}}
                    <li class="nav-item dropdown">

                        <a class="nav-link dropdown-toggle" id="navbarDropdown-User" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user"></i>   
                        </a>

                        <div class="dropdown-menu dropdown-menu-right dropdown-default" aria-labelledby="navbarDropdown-User">
                            <a href="#" class="dropdown-item">{{Auth::user()->name}}</a>
                            
                            <a href="/actividad" class="dropdown-item">Actividad</a>
                            <a href="/mi-contenido">Mi Contenido</a>
                            <a href="/perfil" class="dropdown-item">Perfil</a>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Cerrar sesión</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                                </form>
                        </div>
                    </li>
                    {{-- <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-user"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-default"
                          aria-labelledby="navbarDropdownMenuLink-333">
                          <a class="dropdown-item" href="#">Action</a>
                          <a class="dropdown-item" href="#">Another action</a>
                          <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                      </li> --}}
                    @endguest
                
            </ul>
        </div>
    </div>
</nav>
<!--/.Navbar -->

