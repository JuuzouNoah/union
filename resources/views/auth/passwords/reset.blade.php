@extends('layouts-web.app')

@section('contenido')
<div class="container">
    <div class="row">
        <div class="col-md-7 mt-6 mb-5">
            <div class="jumbotron">
                <h2 class="display-5">Estás a nada de recuperar tu cuenta.</h2>
                <hr class="my-4">
                <p class="lead">Escribe tu nueva contraseña y confirma para poder acceder a tu cuenta.</p>
            </div>
        </div>
        <div class="col-md-5 mt-md-6 mb-5">

            <form class="text-center border border-light p-5" method="POST" action="{{ route('password.update') }}">
                @csrf
                
                <h2 class="display-5 mb-4">Establecer nueva contraseña</h2>


                    <input type="hidden" name="token" value="{{ $token }}">

                    <input id="email" type="email" placeholder="Correo Electrónico" class="form-control mb-4 @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                        <span class="mb-2" role="alert">
                            <p class="red-text">{{ $message }}</p>
                        </span>
                    @enderror

                    <input id="password" type="password" placeholder="Nueva Contraseña" class="form-control mb-4 @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                    @error('password')
                        <span class="mb-2" role="alert">
                            <p class="red-text">{{ $message }}</p>
                        </span>
                    @enderror


                    <input id="password-confirm" type="password" placeholder="Confirmar Contraseña" class="form-control mb-4" name="password_confirmation" required autocomplete="new-password">

                        
                    <button type="submit" class="btn btn-primary my-4">
                        {{ __('Reset Contraseña') }} <i class="fas fa-lock ml-1"></i>
                    </button>

            </form>
        </div>
    </div>
</div>
@endsection
