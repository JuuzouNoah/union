@extends('layouts-web.app')

@section('contenido')
<div class="container">
    <div class="row">
        <div class="col-md-7 mt-6 mb-5">
            <div class="jumbotron">
                <h2 class="display-5">¿Has olvidado tu contraseña?</h2>
                <hr class="my-4">
                <p class="lead">Escribe el correo de la cuenta que has olvidado la contraseña para ayudarte a restablecerla, después ingresa a tu correo electrónico.</p>
                <span class="">Nota: es posible que necesites revisar tu Spam.</span>
            </div>
        </div>
        <div class="col-md-5 mt-md-6 mb-5">

            <form class="text-center border border-light p-5" method="POST" action="{{ route('password.email') }}">
                @csrf

                <h2 class="display-5 mb-4">Restablecer contraseña</h2>

                <input type="email" id="email" name="email" class="form-control mb-4 @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="Correo Electrónico" required autocomplete="email" autofocus>
                @error('email')
                    <span class="mb-2" role="alert">
                        <p class="red-text">{{ $message }}</p>
                    </span>
                @enderror

                <button class="btn btn-primary my-4" type="submit">Enviar link a mi correo electrónico <i class="fas fa-unlock ml-1"></i></button>

            </form>

        </div>
    </div>
</div>
@endsection
