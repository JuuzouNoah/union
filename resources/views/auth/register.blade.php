@extends('layouts-web.app')

@section('contenido')
<div class="container">
    <div class="row">
        <div class="col-md-7 mt-6 mb-5">
            <div class="jumbotron">
                <h2 class="display-5">¿Estás listo para ser parte de la familia?</h2>
                <hr class="my-4">
                <p class="lead">Únete a la comunidad estudiantil más grande alrededor del mundo, recibe apoyo de chicos como tú y ayuda a otros a resolver dudas.</p>
            </div>
            
        </div>
        <div class="col-md-5 mt-md-6 mb-5">
            {{-- <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div> --}}
            <!-- Default form register -->
            <form class="text-center border border-light p-5" form method="POST" action="{{ route('register') }}">
                @csrf

                <h2 class="display-5 mb-4">Registro</h2>

                <div class="form-row mb-4">
                    <div class="col">
                        <!-- First name -->
                        <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Nombre Completo" value="{{ old('name') }}" required autocomplete="name">
                        @error('name')
                            <span class="mb-2" role="alert">
                                <p class="red-text">{{ $message }}</p>
                            </span>
                        @enderror
                    </div>
                </div>

                <!-- E-mail -->
                <input type="email" id="email" name="email" class="form-control mb-4 @error('email') is-invalid @enderror" placeholder="Correo Electrónico" value="{{ old('email') }}" required autocomplete="email">
                @error('email')
                    <span class="mb-2" role="alert">
                        <p class="red-text">{{ $message }}</p>
                    </span>
                @enderror

                <!-- Password -->
                <input type="password" id="password" name="password" class="form-control  @error('password') is-invalid @enderror" placeholder="Contraseña" aria-describedby="defaultRegisterFormPasswordHelpBlock" required autocomplete="new-password">
                <small id="defaultRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
                    Mínimo 8 caracteres.
                </small>
                @error('password')
                    <span class="mb-2" role="alert">
                        <p class="red-text">{{ $message }}</p>
                    </span>
                @enderror

                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña" required autocomplete="new-password">

                <!-- Sign up button -->
                <button class="btn btn-primary my-4" type="submit">Registrarme <i class="fas fa-sign-in-alt"></i></button>

                <!-- Social register -->
                <p>o inicia sesión con:</p>

                <a href="#" class="mx-2" role="button"><i class="fab fa-facebook-f blue-text"></i></a>
                <a href="#" class="mx-2" role="button"><i class="fab fa-google red-text"></i></a>

                <hr>

                <!-- Terms of service -->
                <p>Al clickear en
                    <em>Registrarme</em> estás aceptando
                    <a href="/sobre-nosotros/terminos-condiciones" target="_blank">nuestros términos y condiciones de uso</a>

            </form>
            <!-- Default form register -->
        </div>

    </div>
</div>
@endsection
