@extends('layouts-web.app')

@section('contenido')

<div class="container">
    <div class="row">
        <div class="col-md-7 mt-6 mb-5">
            <div class="jumbotron">
                <h2 class="display-5">¡Hola, es un gusto que estés de vuelta!</h2>
                <hr class="my-4">
                <p class="lead">Comienza a interactuar con la comunidad estudiantil más grande alrededor del mundo. recibiendo apoyo en diferentes temas.</p>
            </div>
        </div>

        {{-- --------------------------------------- --}}

        <div class="col-md-5 mt-md-6 mb-5">
            <!-- Default form login -->
        <form class="text-center border border-light p-5" method="POST" action="{{ route('login') }}">
            @csrf

            <h2 class="display-5 mb-4">Inicio de sesión</h2>

            <!-- Email -->
            <input type="email" id="email" name="email" class="form-control mb-4 @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="Correo Electrónico" required autocomplete="email">
            @error('email')
                <span class="mb-2" role="alert">
                    <p class="red-text">{{ $message }}</p>
                </span>
            @enderror

            <!-- Password -->
            <input type="password" id="password" name="password" class="form-control mb-4 @error('password') is-invalid @enderror" placeholder="Contraseña" required autocomplete="current-password">

            @error('password')
                <span class="mb-2" role="alert">
                    <p class="red-text">{{ $message }}</p>
                </span>
            @enderror

            <div class="d-flex justify-content-around">
                <div>
                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('¿Olvidaste tu contraseña?') }}
                        </a>
                    @endif
                </div>
            </div>

            <!-- Sign in button -->
            <button class="btn btn-primary my-4" type="submit">Ingresar <i class="fas fa-sign-in-alt"></i></button>

            <!-- Register -->
            <p>¿No tienes una cuenta?
                <a href="/register">Regístrate</a>
            </p>

            <!-- Social login -->
            <p>o inicia sesión con:</p>

            <a href="#" class="mx-2" role="button"><i class="fab fa-facebook-f blue-text"></i></a>
            <a href="#" class="mx-2" role="button"><i class="fab fa-google red-text"></i></a>

        </form>
        <!-- Default form login -->

        </div>
        
    </div>
</div>

@endsection
