<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Tu contraseña ha sido restablecida',
    'sent' => 'Hemos enviado un link para restablecer contraseña en tu correo.',
    'throttled' => 'Por favor espera antes de intentar.',
    'token' => 'Este token de restablecimiento de contraseña es inválido.',
    'user' => "No se puede encontrar un usuario con este correo electrónico.",

];
